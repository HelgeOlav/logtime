package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

var lf *os.File
var buf *bufio.Writer

func logger_main() {
	logger_init()
	fmt.Printf("Logging to file %s\n", *logFile)
	// outer loop, loop as many times as specified in numRuns, if > 0
	for *numRuns > 0 || *numRuns == -1 {
		logger_loop()
		time.Sleep(time.Duration(*sleepTime) * time.Millisecond)
		if *numRuns > 0 {
			*numRuns--
		}
		buf.Flush()
	}
	fmt.Fprintf(buf, "%s: logging stopped\n", getTimeStamp())
	buf.Flush()
	lf.Close()
}

// Init loop, open logfile for writing
func logger_init() {
	lf, err := os.OpenFile(*logFile, os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	buf = bufio.NewWriter(lf)
	_, err = fmt.Fprintf(buf, "%s: logging started\n", getTimeStamp())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func logger_loop() {
	s := getTimeStamp()
	fmt.Fprintln(buf, s)
	fmt.Println(s)
}

// get timestamp for use in logging
// source for tips: https://www.golangprograms.com/get-current-date-and-time-in-various-format-in-golang.html
func getTimeStamp() string {
	return time.Now().String()
}
