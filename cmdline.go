package main

import (
	"flag"
	"os"
)

var sleepTime *int
var numRuns *int
var logFile *string

func parseFlags() {
	sleepTime = flag.Int("sleep", 1000, "Sleep time in ms")
	numRuns = flag.Int("runs", -1, "Number of runs, -1 to never stop")
	logFile = flag.String("logfile", "logtime.txt", "File to log to")
	flag.Parse()
	if flag.Parsed() == false {
		flag.Usage()
		os.Exit(1)
	}
}
