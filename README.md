# Introduction

This is a tool to log time regulary to file. The reason for this in case you suspect a server is not working as expected. If you have holes in the logging you know why.

## Run

Run using
```shell
go run .
```

## Cross comile

It is easy to crosscomile to other platforms.
```shell script
env GOOS=windows GOARCH=amd64 go build
```